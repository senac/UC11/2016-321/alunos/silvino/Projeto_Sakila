/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import java.util.List;

/**
 *
 * @author Administrador
 */
public interface DAO<T> {
    
    void salvar(T objeto);
    void atualizar(T objeto) ; 
    void deletar(int id) ; 
    List<T> listarTodos () ; 
    T buscarPorId(int id) ; 
    
    
}
